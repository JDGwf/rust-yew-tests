# Jeff's Yew Tests

This project is my exploration of the [https://yew.rs](https://yew.rs) framework and using modules for standardized components for use across different projects.

Live Path: [https://jdgwf.gitlab.io/rust-yew-tests/](https://jdgwf.gitlab.io/rust-yew-tests/)