use yew::prelude::*;

mod standard_components;
mod pages;
mod main_app;
// use crate::standard_components::tests::test_sheet_global_vars::GlobalVars;
// use crate::standard_components::libs::local_storage_shortcuts::get_local_storage_bool;
// use crate::standard_components::libs::local_storage_shortcuts::get_local_storage_string;

use main_app::MainApp;

#[function_component(App)]
fn app() -> Html {

    // let global_vars_state = use_state(|| GlobalVars {
    //     api_key:  get_local_storage_string( "api_key", "".to_owned() ),
    //     test2:  get_local_storage_string( "test2", "".to_owned() ),
    //     check1:  get_local_storage_bool( "check1", false ),
    //     check2:  get_local_storage_bool( "check2", false ),
    //     check3:  get_local_storage_bool( "check3", false ),
    //     to_dos: Vec::new(),
    // });

    html! {
        // <ContextProvider<GlobalVars>
        //      context={(*global_vars_state).clone()}
        // >
            <MainApp />
        // </ContextProvider<GlobalVars>>
    }
}

fn main() {
    yew::start_app::<App>();
}
