use yew::prelude::*;
use yew_router::prelude::*;

use crate::standard_components::tests::test_sheet_router::TestSheetRouter;
use crate::standard_components::libs::set_document_title::set_document_title;

use crate::pages::main_home::MainHome;
use crate::pages::main_technology::MainTechnology;

#[derive(Clone, Routable, PartialEq)]
enum MainRoute {
    #[at("/rust-yew-tests/")]
    Home,
    #[at("/rust-yew-tests/test-sheet/:sub_route")]
    TestSheetRouter { sub_route: String },
    #[at("/rust-yew-tests/test-sheet/")]
    TestSheetRouterRedirect,
    #[at("/rust-yew-tests/technologies")]
    Technologies,
    #[not_found]
    #[at("/rust-yew-tests/404")]
    NotFound,
}


#[derive(Properties, PartialEq)]
pub struct MainAppProps {

}

pub enum MainAppMessage {
    SetSubmenu(Html),
    ToggleMobileMenu(bool),
    HideMobileMenu(bool),
}

pub struct MainApp {
    submenu: Html,
    show_mobile_menu: bool,
}


fn content_switch(
    routes: &MainRoute,
    submenu_callback: &Callback<Html>,
    _show_mobile_menu: bool,
) -> Html {


    match routes {
        MainRoute::Home => {
            set_document_title("Yew Tests - Home".to_owned());
                html! {
                <MainHome />
            }
        },
        MainRoute::Technologies => {
            set_document_title("Yew Tests - Technologies".to_owned());
            html! {
                <MainTechnology />
            }
        },
        MainRoute::TestSheetRouterRedirect => {
            html! {
                <Redirect<MainRoute> to={MainRoute::TestSheetRouter { sub_route: "home".to_string() }} />
            }
        }
        MainRoute::TestSheetRouter { sub_route: _ } => {
            html! {
                <TestSheetRouter set_submenu={submenu_callback} />
            }
        }
        MainRoute::NotFound => {
            set_document_title("Yew Tests - Not Found :(".to_owned());
            html! { <h1>{ "MainRoute 404" }</h1> }
        }
    }

}

fn top_menu_switch(
    routes: &MainRoute,
    submenu: Html,
    mobile_menu_callback: &Callback<MouseEvent>,
    _show_mobile_menu: bool,
) -> Html {
    let mut home_class_active = "".to_string();
    let mut test_sheet_class_active = "".to_string();
    let mut technologies_class_active = "".to_string();
    match routes {
        MainRoute::Home => {
            home_class_active = "active".to_string();

        },
        MainRoute::TestSheetRouter { sub_route: _ } => {
            test_sheet_class_active = "active".to_string();
        },
        MainRoute::TestSheetRouterRedirect => {

        },
        MainRoute::Technologies => {
            technologies_class_active = "active".to_string();
        },
        MainRoute::NotFound => {

        },
    }

    html! {
        <header>
            <ul class={"top-menu"}>
                <li class={"mobile-menu-button"}>
                    <svg onclick={mobile_menu_callback} stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
                </li>
                <li class={home_class_active}>
                    <Link<MainRoute> to={MainRoute::Home}>{"Home"}</Link<MainRoute>>
                </li>
                <li class={test_sheet_class_active}>
                    <Link<MainRoute> to={MainRoute::TestSheetRouter { sub_route: "home".to_string() }}>{"Test Sheet"}</Link<MainRoute>>
                </li>
                <li class={technologies_class_active}>
                    <Link<MainRoute> to={MainRoute::Technologies}>{"Technologies"}</Link<MainRoute>>
                </li>

            </ul>
            {submenu}

        </header>
    }
}

fn mobile_menu_switch(
    routes: &MainRoute,
    submenu: Html,
    hide_mobile_menu_callback: &Callback<MouseEvent>,
    show_mobile_menu: bool,
) -> Html {
    let mut home_class_active = "".to_string();
    let mut test_sheet_class_active = "".to_string();
    let mut technologies_class_active = "".to_string();
    let mut home_submenu = html! { <></> };
    let mut test_sheet_submenu = html! { <></> };
    let mut technologies_submenu = html! { <></> };
    match routes {
        MainRoute::Home => {
            home_class_active = "active".to_string();
            home_submenu = submenu.clone();
        },
        MainRoute::TestSheetRouter { sub_route: _ } => {
            test_sheet_class_active = "active".to_string();
            test_sheet_submenu = submenu.clone();
        },
        MainRoute::TestSheetRouterRedirect => {

        },
        MainRoute::Technologies => {
            technologies_class_active = "active".to_string();
            technologies_submenu = submenu.clone();
        },
        MainRoute::NotFound => {

        },
    }

    let mut active_class = "mobile-menu";

    if show_mobile_menu {
        active_class = "mobile-menu show-mobile-menu"
    }

    html! {
        <div class={active_class}>
            <ul onclick={hide_mobile_menu_callback} class={"main-menu"}>
                <li class={home_class_active}>
                    <Link<MainRoute> to={MainRoute::Home}>{"Home"}</Link<MainRoute>>
                    {home_submenu}
                </li>
                <li class={test_sheet_class_active}>
                    <Link<MainRoute> to={MainRoute::TestSheetRouter { sub_route: "home".to_string() }}>{"Test Sheet"}</Link<MainRoute>>
                    {test_sheet_submenu}
                </li>
                <li class={technologies_class_active}>
                    <Link<MainRoute> to={MainRoute::Technologies}>{"Technologies"}</Link<MainRoute>>
                    {technologies_submenu}
                </li>

            </ul>

        </div>
    }
}

impl Component for MainApp {
    type Message = MainAppMessage;
    type Properties = MainAppProps;

    fn create(
        _ctx: &Context<Self>
    ) -> Self {
        MainApp {
            submenu: html! { <></> },
            show_mobile_menu: false,
        }
    }

    fn update(
        &mut self,
        _ctx: &Context<Self>,
        msg: MainAppMessage,
    ) -> bool {

        match msg {
            MainAppMessage::ToggleMobileMenu( _new_value ) => {
                self.show_mobile_menu = !self.show_mobile_menu;
                return true;
            }
            MainAppMessage::HideMobileMenu( _new_value ) => {
                self.show_mobile_menu = false;
                return true;
            }
            MainAppMessage::SetSubmenu( new_value ) => {

                let new_value = new_value.clone();
                if format!("${:#?}", self.submenu) != format!("${:#?}", new_value) {
                    self.submenu = new_value.clone();
                    return true;
                } else {
                    return false;
                }

            }
        }
    }

    fn view(
        &self,
        ctx: &Context<Self>
    ) -> Html {

        let submenu = self.submenu.clone();
        let mobile_submenu = self.submenu.clone();
        let show_mobile_menu = self.show_mobile_menu;
        let set_submenu = ctx.link().callback(MainAppMessage::SetSubmenu);
        let toggle_mobile_menu = ctx.link().callback(MainAppMessage::ToggleMobileMenu);
        let hide_mobile_menu = ctx.link().callback(MainAppMessage::HideMobileMenu);

        let on_click_toggle_mobile_menu = Callback::from( move | _e: MouseEvent | {
            toggle_mobile_menu.emit( true );

        });

        let on_click_hide_mobile_menu = Callback::from( move | _e: MouseEvent | {
            hide_mobile_menu.emit( true );

        });

        let mut active_class = "content-pane";

        if show_mobile_menu {
            active_class = "content-pane show-mobile-menu"
        }

        html! {

            <>

                <BrowserRouter>
                    <Switch<MainRoute> render={Switch::render(move |routes| top_menu_switch( routes, submenu.clone(), &on_click_toggle_mobile_menu, show_mobile_menu, ) )} />


                <div class={"position-relative"}>
                <Switch<MainRoute> render={Switch::render(move |routes| mobile_menu_switch( routes, mobile_submenu.clone(), &on_click_hide_mobile_menu, show_mobile_menu, ) )} />
                    <div class={active_class}>
                        <Switch<MainRoute> render={Switch::render(move |routes| content_switch( routes, &set_submenu, show_mobile_menu, ))} />
                        <footer><a href={"https://gitlab.com/JDGwf/rust-yew-tests"}>{"Source Code is on GitLab"}</a></footer>
                    </div>
                </div>
                </BrowserRouter>

            </>
        }
    }
}
