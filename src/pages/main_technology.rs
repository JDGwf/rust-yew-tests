use yew::{function_component, html};

use crate::standard_components::ui::nbsp::Nbsp;

#[function_component(MainTechnology)]
pub fn main_technology() -> Html {
    html! {
        <div class={"main-content"}>
            <h1>{ "Technologies" }</h1>
            <p>{"This web app uses the following technologies:"}</p>

            <ul>
                <li>
                    <a href={"https://www.rust-lang.org"}>{"Rust"}</a>
                    <Nbsp />{"-"}<Nbsp />
                    {"All logic and compilation to .wasm"}
                </li>
                <li>
                    <a href={"https://webassembly.org"}>{"WebAssembly (WASM)"}</a>
                    <Nbsp />{"-"}<Nbsp />
                    {"WebAssembly - a NextGen platform for the web and beyond."}
                </li>
                <li>
                    <a href={"http://yew.rs"}>{"Yew"}</a>
                    <Nbsp />{"-"}<Nbsp />
                    {"A React-like web framework written in rust which compiles to .wasm"}
                </li>
                <li>
                    <a href={"https://trunkrs.dev/"}>{"Trunk"}</a>
                    <Nbsp />{"-"}<Nbsp />
                    {"A building and serving tool which helps make web app development in Yew much easier for the developer."}
                </li>
                <li>
                    <a href={"https://sass-lang.com/"}>{"SCSS"}</a>
                    <Nbsp />{"-"}<Nbsp />
                    {"A CSS precompiler which, for me, makes CSS so much cleaner to edit."}
                </li>
            </ul>
        </div>
    }
}

