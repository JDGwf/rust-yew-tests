use yew::{function_component, html};

#[function_component(MainHome)]
pub fn main_home() -> Html {
    html! {
        <div class={"main-content"}>
            <h1>{ "Home" }</h1>
            <p class={"text-center"}>{"This is my testing project of the Yew.rs framework"}</p>
        </div>
    }
}

