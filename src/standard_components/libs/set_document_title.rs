use web_sys;

pub fn set_document_title(
    new_value: String,
) {
    let document = web_sys::window().unwrap().document().unwrap();

    document.set_title( new_value.as_ref() );

}