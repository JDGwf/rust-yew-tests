use yew::prelude::*;
use web_sys::{HtmlInputElement};

#[derive(Properties, PartialEq)]
pub struct InputCheckboxProps {

    #[prop_or_default]
    pub on_change: Callback<bool>,

    #[prop_or_default]
    pub title: String,

    #[prop_or_default]
    pub checked: bool,

    #[prop_or_default]
    pub image_version: bool,

    #[prop_or_default]
    pub bigger_image: bool,

    #[prop_or_default]
    pub label: String,

    #[prop_or_default]
    pub description: String,

    #[prop_or_default]
    pub inline: bool,

    #[prop_or_default]
    pub label_class: String,

    #[prop_or_default]
    pub input_class: String,

    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub base_path: String,
}

pub enum InputCheckboxMessage {
    OnChange(bool),
}

pub struct InputCheckbox;

impl Component for InputCheckbox {
    type Message = InputCheckboxMessage;
    type Properties = InputCheckboxProps;

    fn create(_ctx: &Context<Self>) -> Self {
        InputCheckbox
    }

    fn update(
        &mut self,
        ctx: &Context<Self>,
        msg: InputCheckboxMessage,
    ) -> bool {
        match msg {
            InputCheckboxMessage::OnChange( new_value ) => {
                ctx.props().on_change.emit( new_value );
                false
            }
        }
    }

    fn view(
        &self,
        ctx: &Context<Self>,
    ) -> Html {
        let on_change = ctx.link().callback(
            |event: Event| {
                let input: HtmlInputElement = event.target_unchecked_into();
                InputCheckboxMessage::OnChange(input.checked())
            }
        );

        let mut description = html!(<></>);
        if ctx.props().description.to_string() != "" {
            description = html!(
                <div class={"small-text"}>
                { ctx.props().description.to_string() }
                </div>
            );
        }

        let mut class_variable = "cursor-pointer ".to_owned() + &ctx.props().label_class.to_string();

        if ctx.props().image_version {
            class_variable = "checkbox-image ".to_owned() + &class_variable;
            if ctx.props().bigger_image {
                class_variable = "checkbox-image bigger-image ".to_owned() + &class_variable;
            }
        }

        html! {
            <label
                class={class_variable}
                title={ctx.props().title.to_string()}
            >
                if ctx.props().checked {
                    <img class={"check-image"} src={ctx.props().base_path.to_owned() + "images/check-yes.png"} />
                } else {
                    <img class={"check-image"} src={ctx.props().base_path.to_owned() + "images/check-no.png"} />
                }
                <input
                    class={ctx.props().input_class.to_string()}
                    type={"checkbox"}
                    checked={ctx.props().checked}
                    onchange={on_change}
                />
                {ctx.props().label.to_string()}

                {description}
                { for ctx.props().children.iter() }
            </label>
        }
    }
}
