use yew::prelude::*;
use super::super::internal::ui::input_label::InputLabel;
use web_sys::{HtmlInputElement};

#[derive(Properties, PartialEq)]
pub struct InputTextProps {

    #[prop_or_default]
    pub on_change: Callback<String>,

    #[prop_or_default]
    pub input_type: String,

    #[prop_or_default]
    pub title: String,

    #[prop_or_default]
    pub value: String,

    #[prop_or_default]
    pub description: String,

    #[prop_or_default]
    pub placeholder: String,

    #[prop_or_default]
    pub label: String,

    #[prop_or_default]
    pub inline: bool,

    #[prop_or_default]
    pub label_class: String,

    #[prop_or_default]
    pub input_class: String,

    #[prop_or_default]
    pub children: Children,

}

pub enum InputTextMessage {
    OnChange(String),
}

pub struct InputText;

impl Component for InputText {
    type Message = InputTextMessage;
    type Properties = InputTextProps;

     fn create(_ctx: &Context<Self>) -> Self {
        InputText {

        }
    }

    fn update(
        &mut self,
        ctx: &Context<Self>,
        msg: InputTextMessage,
    ) -> bool {
        match msg {
            InputTextMessage::OnChange( new_value ) => {
                // self.value += 1;
                ctx.props().on_change.emit( new_value );
                false
            }
        }
    }

    fn view(
        &self,
        ctx: &Context<Self>,
    ) -> Html {

        let on_change = ctx.link().callback(
            |event: InputEvent| {
                let input: HtmlInputElement = event.target_unchecked_into();
                InputTextMessage::OnChange(input.value())
            }
        );

        let mut description = html!(<></>);
        if ctx.props().description.to_string() != "" {
            description = html!(
                <div class={"small-text"}>
                { ctx.props().description.to_string() }
                </div>
            );
        }

        html! {
            <label
                class={ctx.props().label_class.to_string()}
                title={ctx.props().title.to_string()}
            >
                <InputLabel
                    label={ctx.props().label.to_string()}
                    inline={ctx.props().inline}
                />
                <input
                    class={ctx.props().input_class.to_string()}
                    placeholder={ctx.props().placeholder.to_string()}
                    type={ctx.props().input_type.to_string()}
                    value={ctx.props().value.to_string()}
                    oninput={on_change}
                />
                {description}
                { for ctx.props().children.iter() }
            </label>
        }
    }
}
